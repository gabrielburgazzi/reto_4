<div style="text-align: center;">
  <img src="http://academia.devtohack.com/assets/logo-academy-black-c0b4a906430b3143562f01bb94fbf463.svg" alt="Markdownify" width="150">
</div>

Introducción a SQL II
=====================

Muestre la cantidad vendida de cada libro, ordenados descendientemente según la cantidad de ventas. Renombre las columnas como sigue:
- identificacion
- titulo
- autor
- cantidad

Input
-----

- Escriba su sentencia `sql` en el archivo `challenge.rb` para lograr el objetivo del reto.

Output
------

- La lista de libros con el total de sus ventas, ordenados descendientemente según sus ventas totales y con los campos renombrados.

Notas
-----

- Recuerde que debe agrupar los resultados para poder hacer totalizaciones.
- Respete el orden de columnas del enunciado.
- Revise la estructura de la base de datos en el diagrama llamado `diagrama.dia` incluido en el repositorio.
- Para comprobar que la sentencia `sql` es correcta, ejecute el comando `ruby test.rb` en el directorio del reto y verifique si hay errores.
- Ejecute `bundle install` antes de escribir y ejecutar las pruebas.

Criterio de evaluación
----------------------

- La solución (su código) hace lo solicitado en el enunciado.
- El código está organizado y bien indentado.
- Se utilizaron los nombres, nomenclaturas y convenciones sugeridas.
- Se valora la simplicidad y claridad de las soluciones: agregar más de lo requerido no siempre genera valor.

Instrucciones
------------

- Haga un fork del repositorio de este reto.
- Clone el nuevo repositorio en su máquina.
- Resuelva el reto según lo solicitado en el enunciado, tomando en cuenta las notas y la definición de listo del reto.
- Haga commit y push de su código a medida que va avanzando.
- Solicite un merge request del commit final al repositorio del reto cuando haya terminado.
- No modifique los tests.
- Utilizce sólo los conocimientos impartidos en clase.

Definición de listo
--------------------

- El reto debe estar terminado en su totalidad.
- El pipeline del reto en Gitlab debe estar aprobado.
- Debe solicitar un merge request al repositorio del reto (forked) para que pueda ser revisado por los mentores.